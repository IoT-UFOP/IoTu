![alt text](imgs/cabecalho_2-1.png)


# IoT UFOP

Este projeto aborda a temática de Internet das Coisas (Internet of Things – IoT), a qual consiste em fazer com que  “objetos” inteligentes comuniquem-se entre si e tomem decisões sem a necessidade de intervenção humana.

Este projeto visa a elaboração e disponibilização de projetos e material técnico didático sobre a temática Internet das coisas. O objetivo é desenvolver diversos projetos IoT completos de tal forma que professores de disciplinas correlatas possam utilizar os projetos sem se preocuparem com a parte não relacionada da disciplina. Por exemplo, considere um projeto IoT de sensoriamento de temperatura com exibição dos dados coletados na web. A disciplina de desenvolvimento Web poderia apresentar trabalhos práticos que envolvam a visualização dos dados na web sem se preocupar com a parte de sistemas embarcados (o sensoriamento). Assim como a disciplina de sistemas embarcados poderia apresentar os conceitos de obtenção de dados de sensores e não se preocupar com a visualização web.

Portanto, foram desenvolvidos projetos IoT com uma documentação de código aberto detalhada, de fácil entendimento e projetados de forma extensiva. Assim, permite-se que os interessados possam usufruir dos projetos e propor trabalhos, projetos de disciplinas ou mesmo monografias abordando a temática de IoT, abstraindo detalhes que sejam alheios às disciplinas.

**Para acessar a lista completa de projetos clique no link a seguir:   
[Lista de Projetos](https://gitlab.com/snippets/1685586)**

Ou acesse o projeto desejado:
1. [Conhecendo o Raspberry Pi](https://gitlab.com/snippets/1684703)
2. [Acendendo um LED](https://gitlab.com/snippets/1684704)
3. [Piscando um LED](https://gitlab.com/snippets/1685582)
4. [Acendendo um LED via Web com o Node-RED](https://gitlab.com/snippets/1685584)
5. [Utilizando um sensor GPS](https://gitlab.com/snippets/1685585)


Veja também a apresentação do trabalho desenvolvido:

[Internet das Coisas como plataforma multidisciplinar de aprendizagem - X Mostra Pró-Ativa](https://gitlab.com/IoT-UFOP/IoTu/blob/master/Internet%20das%20Coisas%20como%20plataforma%20multidisciplinar%20de%20aprendizagem%20-%20X%20Mostra%20Pr%C3%B3-Ativa.pdf)


_____

*Material produzido por meio do financiamento do Programa Pró-Ativa da [Universidade Federal de Ouro Preto](http://www.ufop.br) Edital 2017.*   
*Aluno responsável: Edgar H. A. Rodrigues;*   
*Coordenadores: [Vinícius F. S. Mota](http://professor.ufop.br/viniciusmota) e Erik de Britto e Silva*   
*Trabalho sob a [licença Creative Commons 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/ "Creative Commons 4.0"), que permite copiar, distribuir e transmitir o trabalho em qualquer suporte ou formato desde que sejam citados a autoria e o licenciante. Está licença não permite o uso para fins comerciais e permite adaptações da obra desde que suas contribuições mantenham a mesma licença que o trabalho original.*

*<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.*